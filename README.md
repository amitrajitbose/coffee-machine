# Coffee Machine

Design a coffee machine which makes different beverages based on set ingredients. 

The initialization of the recipes for each drink should be hard-coded, although it should be relatively easy to add new drinks. The machine should display the ingredient stock (+cost) and menu upon startup, and after every piece of valid user input. 

Drink cost is determined by the combination of ingredients. For example, Coffee is 3 units of coffee (75 cents per), 1 unit of sugar (25 cents per), 1 unit of cream (25 cents per). 

Ingredients and Menu items should be printed in alphabetical order. If the drink is out of stock, it should print accordingly. If the drink is in stock, it should print "Dispensing: ". To select a drink, the user should input a relevant number. 

If they submit "r" or "R" the ingredients should restock, and "q" or "Q" should quit. Blank lines should be ignored, and invalid input should print an invalid input message.

If your directory structure looks like this:

```
.
├── Drink.java
├── DrinkMachine.java
├── Ingredient.java
└── README.txt
```

Then using Java Compiler compile the code like this

`javac DrinkMachine.java`

When your directory structure would look like

```
.
├── Drink.class
├── Drink.java
├── DrinkMachine.class
├── DrinkMachine.java
├── DrinkMachine$OutOfStockException.class
├── Ingredient.class
├── Ingredient.java
└── README.txt
```

This is due to generation of byte-code and class files. Please don't delete them.

Run the code using,

`java DrinkMachine`

> Use 'q' to Quit and 'r' to Restock

Reference : https://codereview.stackexchange.com/questions/83135/designing-a-coffee-machine
